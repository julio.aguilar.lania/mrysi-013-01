<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>Articulos</h2>
        
        <%-- ${articulos} --%>
        <a href="nuevoArticulo"><button>Agregar Artículo</button></a>
        <ul>
        <fmt:setLocale value="en_GB"/>
        <c:forEach var="articulo" items="${articulos}">
            <li>${articulo.id}: <a href="articulo?id=${articulo.id}">${articulo.nombre}</a> a 
                <fmt:formatNumber value="${articulo.precio}" type="currency" pattern="#,##0.00"></fmt:formatNumber></li>
        </c:forEach>
        </ul>
    </body>
</html>
