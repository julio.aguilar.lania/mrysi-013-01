package lania.mrysi.compras.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author LANIA
 */
@Controller
public class ControladorSaludo {
    
    @RequestMapping("/saludo")
    public String saludar(Model model) {
        model.addAttribute("mensaje", "Hola desde Spring");
        return "hola";
    }
    
    // Ejemplo de declaración que permitiría acceder al Request y Response como en un Servlet
    @RequestMapping("/ejemplo")
    public void ejemploApiServlets(HttpServletRequest request, HttpServletResponse response) {
        
    }
}
