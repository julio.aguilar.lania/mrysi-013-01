package lania.mrysi.compras.repositorios;

import java.util.List;
import lania.mrysi.compras.entidades.Articulo;

/**
 *
 * @author jaguilar
 */
public interface RepositorioArticulos {

    List<Articulo> getArticulos();

    Articulo getArticuloPorId(Integer id);

    Articulo guardarArticulo(Articulo art);
}
