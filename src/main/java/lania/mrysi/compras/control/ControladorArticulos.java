package lania.mrysi.compras.control;

import lania.mrysi.compras.repositorios.RepositorioArticulos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author jaguilar
 */
@Controller
public class ControladorArticulos {

    @Autowired
    RepositorioArticulos repoArticulos;

    @RequestMapping("/articulos")
    public String listarArticulos(Model model) {
        model.addAttribute("articulos", repoArticulos.getArticulos());
        return "articulos";
    }

    @RequestMapping(value = "/articulo", params = {"id"})
    public String verArticulo(Model model, @RequestParam("id") Integer idArticulo) {
        model.addAttribute("articulo", repoArticulos.getArticuloPorId(idArticulo));
        return "articulo";
    }

    @RequestMapping("/articulo/{id}")
    public String verArticulo2(Model model, @PathVariable("id") Integer idArticulo) {
        model.addAttribute("articulo", repoArticulos.getArticuloPorId(idArticulo));
        return "articulo";
    }

}
