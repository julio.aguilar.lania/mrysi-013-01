<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>Artículo ${articulo.nombre}</h2>
        
        <p><em>${articulo.descripcion}</em></p>
        <p>Precio: ${articulo.precio}</p>
        <p>etc...</p>
    </body>
</html>
