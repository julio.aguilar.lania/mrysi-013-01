package lania.mrysi.compras.entidades;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author jaguilar
 */
public class Articulo {

    private Integer id;
    private String nombre;
    private String descripcion;
    private BigDecimal precio;
    private Date fechaRegistro;
    private Boolean vigente;

    public Articulo(Integer id, String nombre, BigDecimal precio) {
        this.id = id;
        this.nombre = nombre;
        this.precio = precio;
    }

    public Articulo(Integer id, String nombre, String precio) {
        this.id = id;
        this.nombre = nombre;
        this.precio = new BigDecimal(precio);
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Boolean getVigente() {
        return vigente;
    }

    public void setVigente(Boolean vigente) {
        this.vigente = vigente;
    }

    @Override
    public String toString() {
        return "Articulo{" + "id=" + id + ", nombre=" + nombre + ", precio=" + precio + '}';
    }
    
    
}
