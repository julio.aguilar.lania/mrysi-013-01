package lania.mrysi.compras.repositorios;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lania.mrysi.compras.entidades.Articulo;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jaguilar
 */
@Repository
public class RepositorioArticulosStub implements RepositorioArticulos {

    private Map<Integer, Articulo> articulos;

    public RepositorioArticulosStub() {
        articulos = new HashMap<>();
        this.articulos.put(1, new Articulo(1, "Laptop Acer Aspire A515-51-52BQ", "10999"));
        this.articulos.put(2, new Articulo(2, "All in One Lenovo S200Z", "6899"));
        this.articulos.put(3, new Articulo(3, "Proyector Epson PowerLite X05+", "7999"));
    }

    @Override
    public List<Articulo> getArticulos() {
        return new ArrayList(articulos.values());
    }

    @Override
    public Articulo guardarArticulo(Articulo art) {
        if (art.getId() == null) {
            Integer newId = Collections.max(articulos.keySet()) + 1;
            art.setId(newId);
        }
        articulos.put(art.getId(), art);
        return art;
    }

    @Override
    public Articulo getArticuloPorId(Integer id) {
        return articulos.get(id);
    }
}
